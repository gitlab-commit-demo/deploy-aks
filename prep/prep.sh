#!/bin/bash

# Run this script to configure all everything to use TF

set -e

# exports secrets if available; export otherwise $subscriptionId manually
if [ -f "../creds.sh" ]; then
  source ../creds.sh
fi

az account set --subscription $subscriptionId

# customize those if needed
export rg="demo-tf-rg"
export location="East US"
export sku="Standard_LRS"
export vaultName="demovault$RANDOM$RANDOM"
export saName="demosa$RANDOM$RANDOM"
export scName="demosc$RANDOM$RANDOM"
export spName="demosp$RANDOM$RANDOM"
export appName="demo-rbac-app-$RANDOM$RANDOM"
export appSecret="$(openssl rand -base64 30)"
export clientAppName="demo-rbac-client-$RANDOM$RANDOM"

# creates a new resource group which will be used for the vault and TF state
az group create --name "$rg" \
    --location "$location" \
    --subscription="$subscriptionId"

if test $? -ne 0
then
    echo "resource group couldn't be created..."
	exit
else
    echo "resource group created..."
fi

# creates a vault to store secrets
az keyvault create --name "$vaultName" \
    --resource-group $rg \
    --location "$location" \
    --subscription=$subscriptionId

if test $? -ne 0
then
    echo "vault couldn't be created..."
	exit
else
    echo "vault created..."
fi

# creates storage account used by TF
az storage account create --resource-group $rg \
    --name $saName \
    --sku $sku \
    --encryption-services blob \
    --subscription=$subscriptionId

if test $? -ne 0
then
    echo "storage account couldn't be created..."
	exit
else
    echo "storage account created..."
fi

# gets storage account key
export accountKey=$(az storage account keys list --subscription=$subscriptionId --resource-group $rg --account-name $saName --query [0].value -o tsv )

# creats storage container used by TF
az storage container create --name $scName --account-name $saName --account-key $accountKey

if test $? -ne 0
then
    echo "storage container couldn't be created..."
	exit
else
    echo "storage container created..."
fi

# saves secrets to vault
az keyvault secret set --vault-name $vaultName \
    --name "sa-key" \
    --value "$accountKey"
az keyvault secret set --vault-name $vaultName \
    --name "sa-name" \
    --value "$saName"
az keyvault secret set --vault-name $vaultName \
    --name "sc-name" \
    --value "$scName"

if test $? -ne 0
then
    echo "secrets couldn't be saved..."
	exit
else
    echo "secrets are saved in vault..."
fi

# creates a service principal
export sp=$(az ad sp create-for-rbac --name $spName --role="Contributor" --scopes="/subscriptions/$subscriptionId" --years 99 -o tsv)

if test $? -ne 0
then
    echo "service principal couldn't be created..."
	exit
else
    echo "service principal created..."
fi
# gets id and secret
export spSecret=$(echo $sp | awk '{print $4}')
export spId=$(echo $sp | awk '{print $1}')

# save secrets to vault
az keyvault secret set --vault-name $vaultName \
    --name "sp-id" \
    --value "$spId"
az keyvault secret set --vault-name $vaultName \
    --name "sp-secret" \
    --value "$spSecret"

if test $? -ne 0
then
    echo "secrets couldn't be saved..."
	exit
else
    echo "secrets are saved in vault..."
fi

# creates a Azure AD server app for AKS RBAC
export appUrl="http://$appName"

az ad app create --display-name ${appName} \
    --password "${appSecret}" \
    --identifier-uris "${appUrl}" \
    --reply-urls "${appUrl}" \
    --homepage "${appUrl}"

if test $? -ne 0
then
    echo "app couldn't be created..."
	exit
else
    echo "app created..."
fi

# get app id
export appId=$(az ad app list --display-name $appName --query [].appId -o tsv)

# save secrets to vault
az keyvault secret set --vault-name $vaultName \
    --name "app-id" \
    --value "$appId"

if test $? -ne 0
then
    echo "secrets couldn't be saved..."
	exit
else
    echo "secrets are saved in vault..."
fi

# update the app to claim all group memebrships
az ad app update --id $appId \
    --set groupMembershipClaims=All

if test $? -ne 0
then
    echo "app membership couldn't be updated..."
	exit
else
    echo "app membership updated..."
fi

# add needed roles
az ad app permission add --id $appId \
    --api 00000003-0000-0000-c000-000000000000 \
    --api-permissions e1fe6dd8-ba31-4d61-89e7-88639da4683d=Scope 06da0dbc-49e2-44d2-8312-53f166ab848a=Scope 7ab1d382-f21e-4acd-a863-ba3e13f7da61=Role

if test $? -ne 0
then
    echo "app permission couldn't be updated..."
	exit
else
    echo "app permission updated..."
fi

az ad app permission add --id $appId \
    --api 00000002-0000-0000-c000-000000000000 \
    --api-permissions 311a71cc-e848-46a1-bdf8-97ff7156d8e6=Scope

if test $? -ne 0
then
    echo "app permission couldn't be updated..."
	exit
else
    echo "app permission updated..."
fi

#get oauth permission id
export appOauthId="$(az ad app show --id $appId --query "oauth2Permissions[0].id" -o tsv)"

# save secrets to vault
az keyvault secret set --vault-name $vaultName \
    --name "app-oauth-id" \
    --value "$appOauthId"

if test $? -ne 0
then
    echo "secrets couldn't be saved..."
	exit
else
    echo "secrets are saved in vault..."
fi

az keyvault secret set --vault-name $vaultName \
    --name "app-secret" \
    --value "$appSecret"

if test $? -ne 0
then
    echo "secrets couldn't be saved..."
	exit
else
    echo "secrets are saved in vault..."
fi

# creates a Azure AD client app for RBAC
export clientAppUrl="http://$clientAppName"

az ad app create --display-name $clientAppName \
    --native-app \
    --reply-urls $clientAppUrl \
    --homepage $clientAppUrl

if test $? -ne 0
then
    echo "app couldn't be created..."
	exit
else
    echo "app created..."
fi

# get app id
export clientAppId=$(az ad app list --display-name ${clientAppName} --query [].appId -o tsv)

# save secrets to vault
az keyvault secret set --vault-name $vaultName \
    --name "client-app-id" \
    --value "$clientAppId"

if test $? -ne 0
then
    echo "secrets couldn't be saved..."
	exit
else
    echo "secrets are saved in vault..."
fi

# create service principal
az ad sp create --id $clientAppId

if test $? -ne 0
then
    echo "service principal couldn't be created..."
	exit
else
    echo "service principal created..."
fi

# add permissions
az ad app permission add --id $clientAppId --api $appId --api-permissions $appOauthId=Scope

if test $? -ne 0
then
    echo "app permissions couldn't be added..."
	exit
else
    echo "app permissions added..."
fi

# generates ssh keys
ssh-keygen -t rsa -b 4096 -C "terraform" -f ./id_rsa -N ''

if test $? -ne 0
then
    echo "ssh keys couldn't be created..."
	exit
else
    echo "ssh keys created..."
fi

# save secrets to vault
az keyvault secret set --vault-name $vaultName \
    --name "ssh-key" \
    --file ./id_rsa

if test $? -ne 0
then
    echo "secrets couldn't be saved..."
	exit
else
    echo "secrets are saved in vault..."
fi

az keyvault secret set --vault-name $vaultName \
    --name "ssh-pub-key" \
    --file ./id_rsa.pub

if test $? -ne 0
then
    echo "secrets couldn't be saved..."
	exit
else
    echo "secrets are saved in vault..."
fi

echo
echo "------"
echo "API permissions needs to be granted manually using Azure Portal!"
