# Gitlab Commit Demo

## Demo AKS cluster

This repo is part of my Demo at Gitlab Commit. The repo can be used to build a demo AKS cluster using Gitlab CI/CD.  
A guide on how to install an AKS cluster to host your Gitlab Runner can be found [here](https://gitlab.com/gitlab-commit-demo).

## TL;DR

1. Clone this repo
2. Export following project variables
    - `tenentId` containing your Azure Tenant ID
    - `subscriptionId` containing your Azure subscription
    - `azUser` containing your Azure User
    - `azPsw` containing your Azure User password
    - `runnerToken` containing the Runner token of your GitLab Project/Group
3. Execute `prep/prep.sh` to setup requirements for AKS & Terraform - skip this step if you already deployed [deploy-aks-with-runner](https://gitlab.com/gitlab-commit-demo)
4. Execute the pipeline

> Make sure that you mark your runner with "kubernetes" if you have not created it with the linked repo! Your runner also needs a configured shared cache server!
